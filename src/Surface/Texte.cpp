#include "Surface/Texte.h"

Texte::Texte(const std::string &message, const std::string &fontFile, SDL_Color color, int fontSize, SDL_Renderer *renderer)
{
    //ctor
    m_fontFile = fontFile;
    m_color = color;
    m_fontSize = fontSize;
    m_renderer = renderer;
    setMessage(message);
}

Texte::~Texte()
{
    //dtor
}

int Texte::getLength()
{
    return m_message.size();
}

void Texte::setMessage(const std::string &message)
{
    m_message = message;
    if(message.size() != 0)
        m_tex = renderText(message, m_fontFile, m_color, m_fontSize, m_renderer);
}

void Texte::afficher(int x, int y)
{
    renderTexture(m_tex, m_renderer, x, y);
}

void Texte::afficher(int x, int y, int w, int h)
{
    renderTexture(m_tex, m_renderer, x, y, w, h);
}

SDL_Texture* Texte::renderText(const std::string &message, const std::string &fontFile, SDL_Color color, int fontSize, SDL_Renderer *renderer)
{
    //Open the font
    TTF_Font *font = TTF_OpenFont(fontFile.c_str(), fontSize);
    if (font == nullptr)
    {
        logSDLError(std::cout, "TTF_OpenFont");
        return nullptr;
    }
    //We need to first render to a surface as that's what TTF_RenderText
    //returns, then load that surface into a texture
    SDL_Surface *surf = TTF_RenderText_Blended(font, message.c_str(), color);
    if (surf == nullptr)
    {
        TTF_CloseFont(font);
        logSDLError(std::cout, "TTF_RenderText");
        return nullptr;
    }
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surf);
    if (texture == nullptr)
    {
        logSDLError(std::cout, "CreateTexture");
    }
    //Clean up the surface and font
    SDL_FreeSurface(surf);
    TTF_CloseFont(font);
    return texture;
}

void Texte::logSDLError(std::ostream &os, const std::string &msg)
{
    os << msg << " error: " << SDL_GetError() << std::endl;
}

void Texte::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, int w, int h)
{
    //Setup the destination rectangle to be at the position we want
    m_dst.x = x;
    m_dst.y = y;
    m_dst.w = w;
    m_dst.h = h;
    SDL_RenderCopy(ren, tex, NULL, &m_dst);
}

void Texte::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y)
{
    int w, h;
    SDL_QueryTexture(tex, NULL, NULL, &w, &h);
    renderTexture(tex, ren, x, y, w, h);
}

bool Texte::click()
{
    int x, y;
    SDL_GetMouseState(&x, &y);
    if(SDL_BUTTON(SDL_BUTTON_LEFT) && x > m_dst.x && x < m_dst.x + m_dst.w && y > m_dst.y && y < m_dst.y + m_dst.h)
        return true;
    return false;
}

bool Texte::hover()
{

    int x, y;
    SDL_GetMouseState(&x, &y);
    if(x > m_dst.x && x < m_dst.x + m_dst.w && y > m_dst.y && y < m_dst.y + m_dst.h)
        return true;
    return false;
}
