#include "Surface/Image.h"

Image::Image(const std::string &file, SDL_Renderer *ren)
{
    //ctor
    m_ren = ren;
    m_tex = loadTexture(file, ren);
}

Image::~Image()
{
    //dtor
}

void Image::afficher(int x, int y)
{
    renderTexture(m_tex, m_ren, x, y);
}

void Image::afficher(int x, int y, int w, int h)
{
    renderTexture(m_tex, m_ren, x, y, w, h);
}

bool Image::click()
{
    int x, y;
    SDL_GetMouseState(&x, &y);
    if(SDL_BUTTON(SDL_BUTTON_LEFT) && x > m_dst.x && x < m_dst.x + m_dst.w && y > m_dst.y && y < m_dst.y + m_dst.h)
        return true;
    return false;
}

void Image::logSDLError(std::ostream &os, const std::string &msg)
{
    os << msg << " error: " << SDL_GetError() << std::endl;
}

SDL_Texture* Image::loadTexture(const std::string &file, SDL_Renderer *ren)
{
    SDL_Texture *texture = IMG_LoadTexture(ren, file.c_str());
    if (texture == nullptr)
    {
        logSDLError(std::cout, "LoadTexture");
    }
    return texture;
}

void Image::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, int w, int h)
{
    //Setup the destination rectangle to be at the position we want
    m_dst.x = x;
    m_dst.y = y;
    m_dst.w = w;
    m_dst.h = h;
    SDL_RenderCopy(ren, tex, NULL, &m_dst);
}

void Image::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y)
{
    int w, h;
    SDL_QueryTexture(tex, NULL, NULL, &w, &h);
    renderTexture(tex, ren, x, y, w, h);
}
