#include "Menu/Param.h"

// Classe qui va charger les param�tres utils �
// l'application dans un fichier si ce fichier existe,
// sinon le fichier est cr��
Param::Param()
{
    //ctor
    loadParam();
}

Param::~Param()
{
    //dtor
}

// Cette fonction permet de cr�er le fichier de param�tres si celui ci n'existe pas
void Param::loadParam()
{
    FILE *paramFile = fopen("param.txt", "r");
    char param[128], valeur[128];
    char ligneParam[128];
    if(paramFile != NULL)// Si le fichier existe
        fclose(paramFile);
    else// Sinon on le cr�e
    {
        paramFile = fopen("param.txt", "w");
        if(paramFile != NULL)
        {
            fputs("ResolutionX : 800\nResolutionY : 600\nFullscreen : false\nAvancer : 26\nReculer : 22\nDroite : 7\nGauche : 4", paramFile);
            fclose(paramFile);
        }
        else
            perror("param.txt");
    }
    updateMap();
}

// Permet de r�cup�rer la valeur d'un param�tre dans la map
std::string Param::lireParam(char *par)
{
    std::map<std::string, std::string>::const_iterator
    mit (map_params.begin()),
        mend(map_params.end());
    for(; mit!=mend; ++mit)
    {
        if(strcmp(mit->first.c_str(), par) == 0)
            return mit->second;
    }
}

// Permet de stocker les param�tres dans une map
void Param::updateMap()
{
    FILE *paramFile = fopen("param.txt", "r");
    char param[128], valeur[128];
    char ligneParam[128];

    std::map<std::string, std::string> map_paramsTmp;

    if(paramFile != NULL)
    {
        while(fgets(ligneParam, sizeof ligneParam, paramFile) != NULL)
        {
            sscanf(ligneParam, "%s : %s", param, valeur);
            map_paramsTmp[param] = valeur;
        }
        fclose(paramFile);
    }
    map_params = map_paramsTmp;
}

void Param::update(int resolution, int fullscreen)
{
    char tailleX[5], tailleY[5];
    char param[128], valeur[128];
    char ligne[30];
    char fs[30];
    if(resolution == 1)
    {
        strcpy(tailleX, "800");
        strcpy(tailleY, "600");
    }
    else if(resolution == 2)
    {
        strcpy(tailleX, "1024");
        strcpy(tailleY, "762");
    }
    else if(resolution == 3)
    {
        strcpy(tailleX, "1280");
        strcpy(tailleY, "720");
    }
    else if(resolution == 4)
    {
        strcpy(tailleX, "1600");
        strcpy(tailleY, "900");
    }
    else if(resolution == 5)
    {
        strcpy(tailleX, "1920");
        strcpy(tailleY, "1080");
    }
    if(fullscreen == 1)
        strcpy(fs, "true");
    else
        strcpy(fs, "false");

    FILE *paramFile = fopen("param.txt", "r");
    if(!paramFile)
    {
        printf("Erreur ouverture param.txt");
        return;
    }
    FILE *temp = fopen("param.temp", "w");
    if(!temp)
    {
        printf("Erreur ouverture param.temp");
        return;
    }

    while(fgets(ligne, sizeof ligne, paramFile) != NULL)
    {
        sscanf(ligne, "%s : %s", param, valeur);
        if(strcmp(param, "ResolutionX") == 0)
            fprintf(temp, "%s : %s\n", param, tailleX);
        else if(strcmp(param, "ResolutionY") == 0)
            fprintf(temp, "%s : %s\n", param, tailleY);
        else if(strcmp(param, "Fullscreen") == 0)
            fprintf(temp, "%s : %s\n", param, fs);
        else
            fprintf(temp, "%s : %s\n", param, valeur);
    }
    fclose(paramFile);
    FILE *newFile = fopen("param.txt", "w+");
    if(!newFile)
    {
        printf("Erreur ouverture param.txt");
        return;
    }
    fclose(temp);
    temp = fopen("param.temp", "r");
    while(fgets(ligne, sizeof ligne, temp) != NULL)
        fprintf(newFile, "%s", ligne);

    fclose(temp);
    fclose(newFile);

    updateMap();
}
