#include "Menu/Interface.h"

// Constructeur de la classe Interface
Interface::Interface()
{
    // Initialisation des param�tres
    m_param = Param();

    // R�cup�ration de la r�solution d'�cran
    m_par.tailleFenetreX = atoi(m_param.lireParam("ResolutionX").c_str());
    m_par.tailleFenetreY = atoi(m_param.lireParam("ResolutionY").c_str());

    // R�cup�ration des touches
    m_par.reculer = atoi(m_param.lireParam("Reculer").c_str());
    m_par.avancer = atoi(m_param.lireParam("Avancer").c_str());
    m_par.droite = atoi(m_param.lireParam("Droite").c_str());
    m_par.gauche = atoi(m_param.lireParam("Gauche").c_str());

    init();
    bool terminer(false);
    m_choix = 0;

    while(!terminer)
    {
        switch(m_choix)
        {
        case 0 :
            m_choix = menu();
            break;
        case 1 :
        {
            SceneOpenGL scene = SceneOpenGL();
            scene.setWindow(m_fenetre);
            scene.setParam(m_par);
            scene.initialiserFenetre();
            scene.initGL();
            m_choix = scene.bouclePrincipale();
            init();
            break;
        }
        case 2 :
            //multi
            m_choix = ipMenu();
            break;
        case 3 :
            m_choix = options();
            break;
        case 4 :
            terminer = true;
            break;
        case 5 :
            m_choix = loginIHM();
            break;
        case 6 :
            SceneOpenGL scene = SceneOpenGL();
            scene.setWindow(m_fenetre);
            scene.setParam(m_par);
            scene.setConnexionServeur(cs);
            scene.initialiserFenetre();
            scene.initGL();
            m_choix = scene.bouclePrincipale();
            init();
            break;
        }
    }

    // On quitte la SDL

    SDL_DestroyWindow(m_fenetre);
    SDL_Quit();
}

Interface::~Interface()
{

}

int Interface::loginIHM()
{
    SDL_Event event;
    SDL_StartTextInput();
    bool terminer(false);
    m_choix = 5;
    int coOk = 2;

    std::string loginSaisie = "";

    // Boucle principale
    SDL_Color noir = {0, 0, 0, 255};
    Image fond = Image("images/menu.png", m_renderer);
    Image selecteur = Image("images/selecteur.png", m_renderer);
    Image selecteur2 = Image("images/selecteur2.png", m_renderer);
    Texte texteTitre = Texte("Veuillez entrer un pseudo", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteAdresse = Texte("Pseudo : ", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteIp = Texte("", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteConnexionEnCours = Texte(" ", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteConnexion = Texte("Valider", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteAnnuler = Texte("Se d�connecter", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    while(!terminer)
    {
        SDL_WaitEvent(&event);

        if(event.window.event == SDL_WINDOWEVENT_CLOSE)
        {
            terminer = true;
            cs->envoiePseudo("");
            cs->stop();
        }
        else if(event.type == SDL_TEXTINPUT)
        {
            loginSaisie += event.text.text;
            texteIp = Texte(loginSaisie, "fonts/OpenSans.ttf", noir, 500, m_renderer);
        }
        else if(event.type == SDL_KEYDOWN)
        {
            if(event.key.keysym.sym == 8 && loginSaisie.size() != 0)
            {
                loginSaisie.erase(loginSaisie.size() - 1, 1);
                if(loginSaisie.size() != 0)
                    texteIp = Texte(loginSaisie, "fonts/OpenSans.ttf", noir, 500, m_renderer);
            }
            else if(event.key.keysym.scancode == SDL_SCANCODE_RETURN)
            {
                if(m_choix == 5)
                {
                    texteConnexionEnCours = Texte("V�rification en cours...", "fonts/OpenSans.ttf", noir, 500, m_renderer);
                    cs->envoiePseudo(loginSaisie);
                    m_choix = 6;
                    terminer = true;
                }
                return m_choix;
            }
        }
        else if(event.type == SDL_MOUSEBUTTONUP)
        {
            if(texteConnexion.click())
            {
                texteConnexionEnCours = Texte("V�rification en cours...", "fonts/OpenSans.ttf", noir, 500, m_renderer);
                cs->envoiePseudo(loginSaisie);
                m_choix = 6;
                terminer = true;
            }
            else if(texteAnnuler.click())
            {
                m_choix = 0;
                cs->envoiePseudo("");
                cs->stop();
                terminer = true;
            }
        }
        else if(event.type == SDL_MOUSEMOTION)
        {
            if(texteConnexion.hover())
                m_choix = 5;
            else if(texteAnnuler.hover())
                m_choix = 0;
        }

        SDL_RenderClear(m_renderer);
        fond.afficher(0, 0, m_par.tailleFenetreX, m_par.tailleFenetreY);
        texteTitre.afficher(0.2 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY, 0.6 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY);
        texteAdresse.afficher(0.1 * m_par.tailleFenetreX, 0.25 * m_par.tailleFenetreY, 0.3 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        if(loginSaisie.size() != 0)
            texteIp.afficher(0.5 * m_par.tailleFenetreX, 0.25 * m_par.tailleFenetreY, 0.05 * texteIp.getLength() * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteConnexion.afficher(0.4 * m_par.tailleFenetreX, 0.60 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteAnnuler.afficher(0.4 * m_par.tailleFenetreX, 0.75 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        if(m_choix == 5)
        {
            selecteur.afficher(0.3 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.7 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        else if(m_choix == 0)
        {
            selecteur.afficher(0.3 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.7 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        if(coOk != 2)
        {
            texteConnexionEnCours.afficher(0.3 * m_par.tailleFenetreX, 0.3 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY, 0.4 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        SDL_RenderPresent(m_renderer);
        if(cs->getEtat() == false)
        {
            terminer = true;
            m_choix = 0;
        }
    }
    return m_choix;
}

int Interface::ipMenu()
{
    SDL_Event event;
    SDL_StartTextInput();
    bool terminer(false);
    m_choix = 5;
    int coOk = 2;

    std::string ipSaisie = "";

    // Boucle principale
    SDL_Color noir = {0, 0, 0, 255};
    Image fond = Image("images/menu.png", m_renderer);
    Image selecteur = Image("images/selecteur.png", m_renderer);
    Image selecteur2 = Image("images/selecteur2.png", m_renderer);
    Texte texteTitre = Texte("Veuillez entrer l'adresse du serveur", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteAdresse = Texte("Adresse IP : ", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteIp = Texte(ipSaisie, "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteConnexionEnCours = Texte("Connexion En Cours", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteConnexion = Texte("Se connecter", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteAnnuler = Texte("Annuler", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    while(!terminer)
    {
        SDL_WaitEvent(&event);
        if(event.window.event == SDL_WINDOWEVENT_CLOSE)
            terminer = true;
        else if(event.type == SDL_TEXTINPUT)
        {
            ipSaisie += event.text.text;
            texteIp = Texte(ipSaisie, "fonts/OpenSans.ttf", noir, 500, m_renderer);
        }
        else if(event.type == SDL_KEYDOWN)
        {
            if(event.key.keysym.sym == 8 && ipSaisie.size() != 0)
            {
                ipSaisie.erase(ipSaisie.size() - 1, 1);
                if(ipSaisie.size() != 0)
                    texteIp = Texte(ipSaisie, "fonts/OpenSans.ttf", noir, 500, m_renderer);
            }
            else if(event.key.keysym.scancode == SDL_SCANCODE_RETURN)
            {
                if(m_choix == 5)
                {
                    texteConnexionEnCours = Texte("Connection en cours...", "fonts/OpenSans.ttf", noir, 500, m_renderer);
                    coOk = connexionServeur(ipSaisie);
                    if(coOk == 1)
                    {
                        texteConnexionEnCours = Texte("Connection r�ussi", "fonts/OpenSans.ttf", noir, 500, m_renderer);
                        terminer = true;
                        return 5;
                    }
                    else if(coOk == 0)
                        texteConnexionEnCours = Texte("Impossible de se connecter", "fonts/OpenSans.ttf", noir, 500, m_renderer);
                }
                return m_choix;
            }
        }
        else if(event.type == SDL_MOUSEBUTTONUP)
        {
            if(texteConnexion.click())
            {
                texteConnexionEnCours = Texte("Connection en cours...", "fonts/OpenSans.ttf", noir, 500, m_renderer);
                coOk = connexionServeur(ipSaisie);
                if(coOk == 1)
                {
                    texteConnexionEnCours = Texte("Connection r�ussi", "fonts/OpenSans.ttf", noir, 500, m_renderer);
                    terminer = true;
                    return 5;
                }
                else if(coOk == 0)
                    texteConnexionEnCours = Texte("Impossible de se connecter", "fonts/OpenSans.ttf", noir, 500, m_renderer);
            }
            else if(texteAnnuler.click())
            {
                m_choix = 0;
                terminer = true;
            }
        }
        else if(event.type == SDL_MOUSEMOTION)
        {
            if(texteConnexion.hover())
                m_choix = 5;
            else if(texteAnnuler.hover())
                m_choix = 0;
        }

        SDL_RenderClear(m_renderer);
        fond.afficher(0, 0, m_par.tailleFenetreX, m_par.tailleFenetreY);
        texteTitre.afficher(0.2 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY, 0.6 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY);
        texteAdresse.afficher(0.1 * m_par.tailleFenetreX, 0.25 * m_par.tailleFenetreY, 0.3 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        if(ipSaisie.size() != 0)
            texteIp.afficher(0.4 * m_par.tailleFenetreX, 0.25 * m_par.tailleFenetreY, 0.035 * texteIp.getLength() * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteConnexion.afficher(0.4 * m_par.tailleFenetreX, 0.60 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteAnnuler.afficher(0.4 * m_par.tailleFenetreX, 0.75 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        if(m_choix == 5)
        {
            selecteur.afficher(0.3 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.7 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        else if(m_choix == 0)
        {
            selecteur.afficher(0.3 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.7 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY + 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        if(coOk != 2)
        {
            texteConnexionEnCours.afficher(0.3 * m_par.tailleFenetreX, 0.3 * m_par.tailleFenetreY  + 0.15 * m_par.tailleFenetreY, 0.4 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        SDL_RenderPresent(m_renderer);
    }
    return m_choix;
}

int Interface::connexionServeur(std::string ipSaisie)
{
    cs = new ConnexionServeur(ipSaisie);
    socketAll sa = cs->startConnexion();

    return cs->getConnexion();
}

int Interface::options()
{
    SDL_Event event;
    bool terminer(false);
    m_choix = 31;
    int choixResolution = 0;

    int fullscreen;
    // Boucle principale
    SDL_Color noir = {0, 0, 0, 255};
    Image fond = Image("images/menu.png", m_renderer);
    Image selecteur = Image("images/selecteur.png", m_renderer);
    Image selecteur2 = Image("images/selecteur2.png", m_renderer);
    Texte texteTitre = Texte("Options", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteReso = Texte("R�solution", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteReso2 = Texte("800x600", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteFS = Texte("Plein �cran", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteFS2 = Texte("Non", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteRetour = Texte("Retour", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteValider = Texte("Valider", "fonts/OpenSans.ttf", noir, 500, m_renderer);

    if(strcmp(m_param.lireParam("Fullscreen").c_str(), "true") == 0)
    {
        texteFS2.setMessage("Oui");
        fullscreen = 1;
    }
    else
    {
        texteFS2.setMessage("Non");
        fullscreen = 0;
    }

    if(atoi(m_param.lireParam("ResolutionX").c_str()) == 800)
    {
        choixResolution = 1;
    }
    else if(atoi(m_param.lireParam("ResolutionX").c_str()) == 1024)
    {
        texteReso2.setMessage("1024x762");
        choixResolution = 2;
    }
    else if(atoi(m_param.lireParam("ResolutionX").c_str()) == 1280)
    {
        texteReso2.setMessage("1280x720");
        choixResolution = 3;
    }
    else if(atoi(m_param.lireParam("ResolutionX").c_str()) == 1600)
    {
        texteReso2.setMessage("1600x900");
        choixResolution = 4;
    }
    else if(atoi(m_param.lireParam("ResolutionX").c_str()) == 1920)
    {
        texteReso2.setMessage("1920x1080");
        choixResolution = 5;
    }

    while(!terminer)
    {
        SDL_WaitEvent(&event);
        if(event.window.event == SDL_WINDOWEVENT_CLOSE)
            terminer = true;
        else if(event.type == SDL_KEYDOWN)
        {
            if(event.key.keysym.scancode == m_par.reculer && m_choix < 35)
                m_choix++;
            else if(event.key.keysym.scancode == m_par.avancer && m_choix > 31)
                m_choix--;
            else if(event.key.keysym.scancode == m_par.droite)
            {
                switch(m_choix)
                {
                case 31 :
                    if(choixResolution == 1)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1024x762");
                    }
                    else if(choixResolution == 2)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1280x720");
                    }
                    else if(choixResolution == 3)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1600x900");
                    }
                    else if(choixResolution == 4)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1920x1080");
                    }
                    else if(choixResolution == 5)
                    {
                        choixResolution = 1;
                        texteReso2.setMessage("800x600");
                    }
                    break;
                case 32 :
                    if(fullscreen == 0)
                    {
                        texteFS2.setMessage("Oui");
                        fullscreen = 1;
                    }
                    else
                    {
                        texteFS2.setMessage("Non");
                        fullscreen = 0;
                    }
                    break;
                }
            }
            else if(event.key.keysym.scancode == m_par.gauche)
            {
                switch(m_choix)
                {
                case 31 :
                    if(choixResolution == 1)
                    {
                        choixResolution = 5;
                        texteReso2.setMessage("1920x1080");
                    }
                    else if(choixResolution == 2)
                    {
                        choixResolution--;
                        texteReso2.setMessage("800x600");
                    }
                    else if(choixResolution == 3)
                    {
                        choixResolution--;
                        texteReso2.setMessage("1024x762");
                    }
                    else if(choixResolution == 4)
                    {
                        choixResolution--;
                        texteReso2.setMessage("1280x720");
                    }
                    else if(choixResolution == 5)
                    {
                        choixResolution--;
                        texteReso2.setMessage("1600x900");
                    }
                    break;
                case 32 :
                    if(fullscreen == 0)
                    {
                        texteFS2.setMessage("Oui");
                        fullscreen = 1;
                    }
                    else
                    {
                        texteFS2.setMessage("Non");
                        fullscreen = 0;
                    }
                    break;
                }
            }
            else if(event.key.keysym.scancode == SDL_SCANCODE_RETURN)
                if(m_choix == 34)
                    return 0;
                else if(m_choix == 35)
                {
                    m_param.update(choixResolution, fullscreen);
                    updateFenetre();
                    return 0;
                }
        }
        else if(event.type == SDL_MOUSEMOTION)
        {
            if(texteReso.hover() || texteReso2.hover())
                m_choix = 31;
            else if(texteFS.hover() || texteFS2.hover())
                m_choix = 32;
            else if(texteRetour.hover())
                m_choix = 34;
            else if(texteValider.hover())
                m_choix = 35;
        }
        else if(event.type == SDL_MOUSEBUTTONUP)
        {

            if(selecteur.click())
            {
                if(m_choix == 31)
                {
                    if(choixResolution == 1)
                    {
                        choixResolution = 5;
                        texteReso2.setMessage("1920x1080");
                    }
                    else if(choixResolution == 2)
                    {
                        choixResolution--;
                        texteReso2.setMessage("800x600");
                    }
                    else if(choixResolution == 3)
                    {
                        choixResolution--;
                        texteReso2.setMessage("1024x762");
                    }
                    else if(choixResolution == 4)
                    {
                        choixResolution--;
                        texteReso2.setMessage("1280x720");
                    }
                    else if(choixResolution == 5)
                    {
                        choixResolution--;
                        texteReso2.setMessage("1600x900");
                    }
                }
                else if(m_choix == 32)
                {
                    if(fullscreen == 0)
                    {
                        texteFS2.setMessage("Oui");
                        fullscreen = 1;
                    }
                    else
                    {
                        texteFS2.setMessage("Non");
                        fullscreen = 0;
                    }
                }
            }
            else if(selecteur2.click())
            {
                if(m_choix == 31)
                {
                    if(choixResolution == 1)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1024x762");
                    }
                    else if(choixResolution == 2)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1280x720");
                    }
                    else if(choixResolution == 3)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1600x900");
                    }
                    else if(choixResolution == 4)
                    {
                        choixResolution++;
                        texteReso2.setMessage("1920x1080");
                    }
                    else if(choixResolution == 5)
                    {
                        choixResolution = 1;
                        texteReso2.setMessage("800x600");
                    }
                }
                else if(m_choix == 32)
                {
                    if(fullscreen == 0)
                    {
                        texteFS2.setMessage("Oui");
                        fullscreen = 1;
                    }
                    else
                    {
                        texteFS2.setMessage("Non");
                        fullscreen = 0;
                    }
                }
            }
            else if(texteRetour.click())
                return 0;
            else if(texteValider.click())
            {
                m_param.update(choixResolution, fullscreen);
                updateFenetre();
                return 0;
            }
        }
        SDL_RenderClear(m_renderer);
        fond.afficher(0, 0, m_par.tailleFenetreX, m_par.tailleFenetreY);
        texteTitre.afficher(0.4 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY);
        texteReso.afficher(0.2 * m_par.tailleFenetreX, 0.3 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteReso2.afficher(0.6 * m_par.tailleFenetreX, 0.3 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteFS.afficher(0.2 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteFS2.afficher(0.6 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteRetour.afficher(0.2 * m_par.tailleFenetreX, 0.75 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteValider.afficher(0.6 * m_par.tailleFenetreX, 0.75 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);

        if(m_choix == 31 || m_choix == 32)
        {
            selecteur.afficher(0.5 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + (m_choix - 31) * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.9 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + (m_choix - 31) * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        else if(m_choix == 33)
        {
            selecteur.afficher(0.2 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + (m_choix - 31) * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.8 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + (m_choix - 31) * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        else if(m_choix == 34)
        {
            selecteur.afficher(0.1 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + 3 * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.5 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + 3 * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        else if(m_choix == 35)
        {
            selecteur.afficher(0.5 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + 3 * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
            selecteur2.afficher(0.9 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + 3 * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        }
        SDL_RenderPresent(m_renderer);
    }
    return m_choix;
}

int Interface::menu()
{
    SDL_Event event;
    bool terminer(false);
    m_choix = 1;
    // Boucle principale
    SDL_Color noir = {0, 0, 0, 255};
    Image fond = Image("images/menu.png", m_renderer);
    Image selecteur = Image("images/selecteur.png", m_renderer);
    Image selecteur2 = Image("images/selecteur2.png", m_renderer);
    Texte texteTitre = Texte("Menu", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteSolo = Texte("Solo", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteMulti = Texte("Multi", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteOptions = Texte("Options", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    Texte texteQuitter = Texte("Quitter", "fonts/OpenSans.ttf", noir, 500, m_renderer);
    while(!terminer)
    {
        SDL_PollEvent(&event);

        if(event.window.event == SDL_WINDOWEVENT_CLOSE)
            terminer = true;
        else if(event.type == SDL_KEYDOWN)
        {
            if(event.key.keysym.scancode == m_par.reculer && m_choix < 4)
                m_choix++;
            else if(event.key.keysym.scancode == m_par.avancer && m_choix > 1)
                m_choix--;
            else if(event.key.keysym.scancode == SDL_SCANCODE_RETURN)
                terminer = true;
        }
        else if(event.type == SDL_MOUSEMOTION)
        {
            if(texteSolo.hover())
                m_choix = 1;
            else if(texteMulti.hover())
                m_choix = 2;
            else if(texteOptions.hover())
                m_choix = 3;
            else if(texteQuitter.hover())
                m_choix = 4;
        }
        else if(event.type == SDL_MOUSEBUTTONUP)
        {
            if(texteSolo.click())
            {
                m_choix = 1;
                terminer = true;
            }
            else if(texteMulti.click())
            {
                m_choix = 2;
                terminer = true;
            }
            else if(texteOptions.click())
            {
                m_choix = 3;
                terminer = true;
            }
            else if(texteQuitter.click())
            {
                m_choix = 4;
                terminer = true;
            }
        }

        SDL_RenderClear(m_renderer);
        fond.afficher(0, 0, m_par.tailleFenetreX, m_par.tailleFenetreY);
        texteTitre.afficher(0.4 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.1 * m_par.tailleFenetreY);
        texteSolo.afficher(0.4 * m_par.tailleFenetreX, 0.3 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteMulti.afficher(0.4 * m_par.tailleFenetreX, 0.45 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteOptions.afficher(0.4 * m_par.tailleFenetreX, 0.60 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        texteQuitter.afficher(0.4 * m_par.tailleFenetreX, 0.75 * m_par.tailleFenetreY, 0.2 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        selecteur.afficher(0.3 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + (m_choix - 1) * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        selecteur2.afficher(0.7 * m_par.tailleFenetreX - 0.025 * m_par.tailleFenetreX, 0.32 * m_par.tailleFenetreY  + (m_choix - 1) * 0.15 * m_par.tailleFenetreY, 0.05 * m_par.tailleFenetreX, 0.15 * m_par.tailleFenetreY);
        SDL_RenderPresent(m_renderer);
    }
    return m_choix;
}

// Initialisation de la SDL
void Interface::init()
{
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << std::endl;
        SDL_Quit();
    }

    paramFenetre();

    if(m_fenetre == 0)
    {
        std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl;
        SDL_Quit();
    }

    m_renderer = SDL_CreateRenderer(m_fenetre, -1, 0);

    if(!m_renderer)
    {
        std::cout << "SDL Error : " << SDL_GetError() << std::endl;
    }

    if((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG)
    {
        SDL_Quit();
    }

    if(TTF_Init() != 0)
    {
        SDL_Quit();
    }
}

void Interface::updateFenetre()
{
    m_par.tailleFenetreX = atoi(m_param.lireParam("ResolutionX").c_str());
    m_par.tailleFenetreY = atoi(m_param.lireParam("ResolutionY").c_str());
    m_par.reculer = atoi(m_param.lireParam("Reculer").c_str());
    m_par.avancer = atoi(m_param.lireParam("Avancer").c_str());
    m_par.droite = atoi(m_param.lireParam("Droite").c_str());
    m_par.gauche = atoi(m_param.lireParam("Gauche").c_str());
    SDL_DestroyWindow(m_fenetre);
    init();
}

// Chargement de la fenetre en fonction des param�tres du fichier param
void Interface::paramFenetre()
{
    if(strcmp(m_param.lireParam("Fullscreen").c_str(), "false") == 0)
        m_fenetre = SDL_CreateWindow("Client", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_par.tailleFenetreX, m_par.tailleFenetreY, SDL_WINDOW_OPENGL);
    else if(strcmp(m_param.lireParam("Fullscreen").c_str(), "true") == 0)
        m_fenetre = SDL_CreateWindow("Client", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_par.tailleFenetreX, m_par.tailleFenetreY, SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN);
    else
        printf("Erreur fullscreen");
}
