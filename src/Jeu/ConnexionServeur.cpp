#include "Jeu/ConnexionServeur.h"

/**
** Classe permettant de g�rer la connexion avec le serveur
**/

ConnexionServeur::ConnexionServeur(std::string ip)
{
    m_ip = ip;
    //ctor
}

ConnexionServeur::~ConnexionServeur()
{
    //dtor
}

int ConnexionServeur::getConnexion()
{
    return m_coOk;
}

socketAll ConnexionServeur::startConnexion()
{
    #if defined (WIN32)
        WSADATA WSAData;
        int erreur = WSAStartup(MAKEWORD(2,2), &WSAData);
    #else
        int erreur = 0;
    #endif

    SOCKADDR_IN sin;

    m_sockAll.sockDeco = 0;
    m_sockAll.sockPos = 0;
    m_sockAll.sockBalle = 0;
    m_sockAll.sockSentinelle = 0;
    m_coOk = 0;

    if(!erreur)
    {
        m_sockAll.sockDeco = socket(AF_INET, SOCK_STREAM, 0);
        m_sockAll.sockPos = socket(AF_INET, SOCK_STREAM, 0);
        m_sockAll.sockBalle = socket(AF_INET, SOCK_STREAM, 0);
        m_sockAll.sockSentinelle = socket(AF_INET, SOCK_STREAM, 0);

        sin.sin_addr.s_addr = inet_addr(m_ip.c_str());
        sin.sin_family = AF_INET;
        sin.sin_port = htons(PORT);

        if(connect(m_sockAll.sockDeco, (SOCKADDR*)&sin, sizeof(sin)) != SOCKET_ERROR)
        {
            connect(m_sockAll.sockPos, (SOCKADDR*)&sin, sizeof(sin));
            connect(m_sockAll.sockBalle, (SOCKADDR*)&sin, sizeof(sin));
            connect(m_sockAll.sockSentinelle, (SOCKADDR*)&sin, sizeof(sin));
            std::thread(&ConnexionServeur::startSentinelle, this).detach();
            std::thread(&ConnexionServeur::startSentinelleRecep, this).detach();
            m_coOk = 1;
        }
        else
            printf("Impossible de se connecter");
    }
    return m_sockAll;
}

void ConnexionServeur::startSentinelle()
{
    m_sentinelle = true;
    while(m_sentinelle)
    {
        send(m_sockAll.sockSentinelle, "1", 1, 0);
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void ConnexionServeur::lastCheck()
{
    char buffer[2];
    while(m_sentinelle)
    {
        if(recv(m_sockAll.sockSentinelle, buffer, 2, 0) != SOCKET_ERROR)
        {
            m_lastCheck = std::chrono::system_clock::now();
        }
    }
}

// Fonction permettant de checker que la connexion avec le serveur est active
void ConnexionServeur::startSentinelleRecep()
{
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    m_lastCheck = std::chrono::system_clock::now();
    std::thread(&ConnexionServeur::lastCheck, this).detach();
    while(m_sentinelle)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));
        now = std::chrono::system_clock::now();
        int dur = std::chrono::duration_cast<std::chrono::seconds>(now-m_lastCheck).count();
        if(dur >= 4)
            m_sentinelle = false;
    }
    send(m_sockAll.sockDeco, "1", 1, 0);
}

void ConnexionServeur::envoiePseudo(std::string texte)
{
    m_pseudo = texte;
    send(m_sockAll.sockDeco, m_pseudo.c_str(), 10, 0);
}

void ConnexionServeur::envoiePos(glm::vec3* position, float* angle)
{
    m_sendPos = false;
    while(getEtat())
    {
        if(m_sendPos)
        {
            positionJoueur sockPosition;
            strcpy(sockPosition.pseudo, m_pseudo.c_str());
            sockPosition.posX = position->x;
            sockPosition.posY = position->y;
            sockPosition.posZ = position->z;
            sockPosition.angle = *angle * 57,2958;
            send(m_sockAll.sockPos, (char *)&sockPosition, sizeof(sockPosition), 0);
            m_sendPos = false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

bool ConnexionServeur::getSendPos()
{
    return m_sendPos;
}

void ConnexionServeur::setSendPos(bool b)
{
    m_sendPos = b;
}

void ConnexionServeur::envoieBalle(glm::vec3* position, glm::vec3* orientation, float hauteurCamera)
{
    m_sendBalle = false;
    while(getEtat())
    {
        if(m_sendBalle)
        {
            socketBalle soBa;
            soBa.posX = position->x;
            soBa.posY = position->y;
            soBa.posZ = position->z;
            soBa.oriX = orientation->x;
            soBa.oriY = orientation->y;
            soBa.oriZ = orientation->z;
            send(m_sockAll.sockBalle, (char *)&soBa, sizeof(soBa), 0);
            m_sendBalle = false;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

bool ConnexionServeur::getSendBalle()
{
    return m_sendBalle;
}

void ConnexionServeur::setSendBalle(bool b)
{
    m_sendBalle = b;
}

void ConnexionServeur::deplacementThread(std::list<positionJoueur>* listeJoueur)
{
    positionJoueur pos;
    while(m_sentinelle && recv(m_sockAll.sockPos, (char *)&pos, sizeof(pos), 0) != SOCKET_ERROR)
    {
        std::list<positionJoueur>::iterator it;
        bool done = false;
        if(listeJoueur->size() != 0)
        {
            for(it = listeJoueur->begin(); it != listeJoueur->end() && !done; it++)
            {
                if(!strcmp((*it).pseudo, pos.pseudo))
                {
                    done = true;
                    (*it) = pos;
                }
            }
        }
        if(!done)
            listeJoueur->push_back(pos);
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

}

void ConnexionServeur::deconnectionThread(std::list<positionJoueur>* listeJoueur)
{
    char pseudoBuffer[20];
    while(m_sentinelle && recv(m_sockAll.sockDeco, pseudoBuffer, 20, 0) != SOCKET_ERROR)
    {
        std::list<positionJoueur>::iterator it;
        std::string pseudo = pseudoBuffer;
        bool done = false;
        for(it = listeJoueur->begin(); it != listeJoueur->end() && !done; it++)
        {
            if(!strcmp((*it).pseudo, pseudo.c_str()))
            {
                strcpy((*it).pseudo, "");
                done = true;
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void ConnexionServeur::balleThread()
{
    while(m_sentinelle && recv(m_sockAll.sockBalle, (char *)&m_socketBalle, sizeof(socketBalle), 0) != SOCKET_ERROR)
    {
        *m_balle = 2;
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

int* ConnexionServeur::getEtatBalle()
{
    return m_balle;
}

void ConnexionServeur::reception(std::list<positionJoueur>* listeJoueur)
{
    m_balle = new int;
    std::thread(&ConnexionServeur::deplacementThread, this, listeJoueur).detach();
    std::thread(&ConnexionServeur::deconnectionThread, this, listeJoueur).detach();
    std::thread(&ConnexionServeur::balleThread, this).detach();
}

void ConnexionServeur::stop()
{
    m_sentinelle = false;
}

bool ConnexionServeur::getEtat()
{
    return m_sentinelle;
}

socketBalle* ConnexionServeur::getBalle()
{
    return &m_socketBalle;
}
