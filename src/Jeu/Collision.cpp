#include "Collision.h"

/**
** Classe permettant de g�rer les collisions
**/

Collision::Collision()
{
    //ctor
}

Collision::~Collision()
{
    //dtor
}

void Collision::setTerrain(std::vector<float> terrainVertices, int vertices)
{
    m_terrainVertices = terrainVertices;
    m_vertices = vertices;
}

bool Collision::collisionTerrain(float x, float y, float z)
{
    Point p1, p2, p3, pi;
    Triangle triangle;
    pi.X = x;
    pi.Y = y;
    pi.Z = z;
    for(int i = 0; i < m_vertices * 3; i=i+9)
    {
        p1.X = m_terrainVertices[i];
        p1.Y = m_terrainVertices[i+1];
        p1.Z = m_terrainVertices[i+2];
        triangle.point1 = p1;
        p2.X = m_terrainVertices[i+3];
        p2.Y = m_terrainVertices[i+4];
        p2.Z = m_terrainVertices[i+5];
        triangle.point2 = p2;
        p3.X = m_terrainVertices[i+6];
        p3.Y = m_terrainVertices[i+7];
        p3.Z = m_terrainVertices[i+8];
        triangle.point3 = p3;

        float t = getDistanceToPlane(triangle, pi);
        bool y = pointInTriangle(pi, p1, p2, p3);

        if(t < 0.8 && t > -0.8 && y)
            return true;
    }
    return false;
}

float Collision::getDistanceToPlane(Triangle triangle, Point point)
{
    Point a(triangle.point1);
    Point b(triangle.point2);
    Point c(triangle.point3);
    glm::vec3 ab(b.X - a.X, b.Y - a.Y, b.Z - a.Z);
    glm::vec3 ac(c.X - a.X, c.Y - a.Y, c.Z - a.Z);
    glm::vec3 n = glm::cross(ab, ac);

    n=glm::normalize(n);

    return n.x*point.X + n.y*point.Y + n.z*point.Z - (n.x * a.X + n.y * a.Y + n.z * a.Z);
}

bool Collision::pointInTriangle(Point p, Point a, Point b, Point c)
{
    if(sameSide(p, a, b, c) && sameSide(p, b, a, c) && sameSide(p, c, a, b))
        return true;
    return false;
}


bool Collision::sameSide(Point p1, Point p2, Point a, Point b)
{
    glm::vec3 p1v(p1.X, p1.Y, p1.Z);
    glm::vec3 p2v(p2.X, p2.Y, p2.Z);
    glm::vec3 av(a.X, a.Y, a.Z);
    glm::vec3 bv(b.X, b.Y, b.Z);

    glm::vec3 cp1 = glm::cross(bv-av, p1v-av);

    glm::vec3 cp2 = glm::cross(bv-av, p2v-av);
    if(glm::dot(cp1, cp2) >= 0)
        return true;
    return false;
}
