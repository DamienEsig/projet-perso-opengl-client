#include "Terrain.h"

/**
** Classe utilis�e pour le terrain
**/

Terrain::Terrain()
{
    //ctor
}

Terrain::~Terrain()
{
    //dtor
}

std::vector<float> Terrain::getTableauVertices()
{
    return m_loadObjectMur.getTableauObjVertices();
}

int Terrain::getNombreVertices()
{
    return m_loadObjectMur.nombreVertices();
}

void Terrain::setSol(std::string pathFile)
{
    m_loadObjectSol = LoadObject();
    m_loadObjectSol.setPathFile(pathFile);
    m_loadObjectSol.loadMesh();
}

void Terrain::setMur(std::string pathFile)
{
    m_loadObjectMur = LoadObject();
    m_loadObjectMur.setPathFile(pathFile);
    m_loadObjectMur.loadMesh();
}

void Terrain::setShader(std::string vertexShader, std::string fragmentShader)
{
    m_loadObjectSol.loadShader(vertexShader, fragmentShader);
    m_loadObjectMur.loadShader(vertexShader, fragmentShader);
}

void Terrain::setTextureSol(std::string pathTexture)
{
    m_loadObjectSol.loadTexture(pathTexture);
}

void Terrain::setTextureMur(std::string pathTexture)
{
    m_loadObjectMur.loadTexture(pathTexture);
}

void Terrain::afficherAvecTextures(glm::mat4& projection, glm::mat4& modelview)
{
    glm::mat4 savedModelview = modelview;
    modelview = glm::translate(modelview, m_position);
    modelview = glm::rotate(modelview, m_angleTheta, glm::vec3(0.0, 1.0, 0.0));
    m_loadObjectSol.afficherAvecTextures(projection, modelview);
    m_loadObjectMur.afficherAvecTextures(projection, modelview);
    modelview = savedModelview;
}
