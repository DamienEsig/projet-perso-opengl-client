#include "Jeu/Object/LoadObject.h"

/**
** Classe permettant de charger les objets � afficher
**/

LoadObject::LoadObject() : m_texture("textures/balle.jpg"), m_shader("shaders/texture.vert", "shaders/texture.frag")
{
    //ctor
}

LoadObject::~LoadObject()
{
    //dtor
}

void LoadObject::setPathFile(std::string pathFile)
{
    m_pathFile = pathFile;
}

void LoadObject::loadShader(std::string vertexShader, std::string fragmentShader)
{
    m_shader = Shader(vertexShader, fragmentShader);
    m_shader.charger();
}

void LoadObject::loadTexture(std::string pathTexture)
{
    m_texture = Texture(pathTexture);
    m_texture.charger();
}

void LoadObject::loadTextureSDLSurface(std::string texte)
{
    SDL_Color rouge = {255, 0, 0, 255};
    TTF_Font *font = TTF_OpenFont("fonts/Arial.ttf", 200);
    SDL_Surface *surf = TTF_RenderText_Blended(font, texte.c_str(), rouge);
    m_texture.chargerFromSDLSurface(surf);
    TTF_CloseFont(font);
}

// Fonction permettant de charger les vertices pr�sent dans le fichier obj
void LoadObject::loadMesh()
{
    FILE* meshFile = fopen(m_pathFile.c_str(), "r");
    if(meshFile != NULL)
    {
        int nombrePointVertices = 0;
        int nombrePointTexture = 0;
        int nombrePointNormale = 0;
        m_nombrePointFace = 0;
        char chaine[300];
        while (fgets(chaine, 300, meshFile) != NULL) // On lit le fichier tant qu'on ne re�oit pas d'erreur (NULL)
        {
            if(chaine[0] == 'v') {
                if(chaine[1] == ' ')
                    nombrePointVertices = nombrePointVertices + 3;//pointVertices
                else if(chaine[1] == 't')
                    nombrePointTexture = nombrePointTexture + 2;//pointTexture
                else if(chaine[1] == 'n')
                    nombrePointNormale = nombrePointNormale + 3;//pointTexture
            }
            if(chaine[0] == 'f') {
                m_nombrePointFace = m_nombrePointFace + 3;//nombreFace
            }
        }

        std::vector<float> tableauAllPointVertices;
        std::vector<float> tableauAllPointTexture;
        std::vector<float> tableauAllPointNormale;
        rewind(meshFile);

        int pointVertices = 0;
        int pointTexture = 0;
        int pointNormale = 0;

        while(fgets(chaine, 300, meshFile) != NULL)
        {
            if(chaine[0] == 'v') {
                if(chaine[1] == ' ') {
                    float nb1, nb2, nb3;
                    sscanf(chaine, "v %f %f %f", &nb1, &nb2, &nb3);
                    tableauAllPointVertices.push_back(nb1);
                    tableauAllPointVertices.push_back(nb2);
                    tableauAllPointVertices.push_back(nb3);
                    pointVertices = pointVertices + 3;
                }
                else if(chaine[1] == 't') {
                    float nb1, nb2;
                    sscanf(chaine, "vt %f %f", &nb1, &nb2);
                    tableauAllPointTexture.push_back(nb1);
                    tableauAllPointTexture.push_back(nb2);
                    pointTexture = pointTexture + 2;
                }
                else if(chaine[1] == 'n') {
                    float nb1, nb2, nb3;
                    sscanf(chaine, "v %f %f %f", &nb1, &nb2, &nb3);
                    tableauAllPointNormale.push_back(nb1);
                    tableauAllPointNormale.push_back(nb2);
                    tableauAllPointNormale.push_back(nb3);
                    pointNormale = pointNormale + 3;
                }
            }
        }
        rewind(meshFile);
        while(fgets(chaine, 300, meshFile) != NULL)
        {
            if(chaine[0] == 'f') {
                int sommetVertices1 = 0;
                int sommetTexture1 = 0;
                int sommetNormale1 = 0;
                int sommetVertices2 = 0;
                int sommetTexture2 = 0;
                int sommetNormale2 = 0;
                int sommetVertices3 = 0;
                int sommetTexture3 = 0;
                int sommetNormale3 = 0;


                sscanf(chaine, "f %d/%d/%d %d/%d/%d %d/%d/%d", &sommetVertices1, &sommetTexture1, &sommetNormale1, &sommetVertices2, &sommetTexture2, &sommetNormale2, &sommetVertices3, &sommetTexture3, &sommetNormale3);

                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices1-1)*3]);
                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices1-1)*3 + 1]);
                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices1-1)*3 + 2]);

                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices2-1)*3]);
                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices2-1)*3 + 1]);
                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices2-1)*3 + 2]);

                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices3-1)*3]);
                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices3-1)*3 + 1]);
                m_tableauObjVertices.push_back(tableauAllPointVertices[(sommetVertices3-1)*3 + 2]);

                m_tableauObjTexture.push_back(tableauAllPointTexture[(sommetTexture1-1)*2]);
                m_tableauObjTexture.push_back(tableauAllPointTexture[(sommetTexture1-1)*2 + 1]);

                m_tableauObjTexture.push_back(tableauAllPointTexture[(sommetTexture2-1)*2]);
                m_tableauObjTexture.push_back(tableauAllPointTexture[(sommetTexture2-1)*2 + 1]);

                m_tableauObjTexture.push_back(tableauAllPointTexture[(sommetTexture3-1)*2]);
                m_tableauObjTexture.push_back(tableauAllPointTexture[(sommetTexture3-1)*2 + 1]);

                //

                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale1-1)*3]);
                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale1-1)*3 + 1]);
                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale1-1)*3 + 2]);

                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale2-1)*3]);
                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale2-1)*3 + 1]);
                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale2-1)*3 + 2]);


                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale3-1)*3]);
                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale3-1)*3 + 1]);
                m_tableauObjNormales.push_back(tableauAllPointNormale[(sommetNormale3-1)*3 + 2]);
            }
        }
        fclose(meshFile);
    }
    else
        std::cout<<"non trouve "<<m_pathFile.c_str()<<std::endl;
}

int LoadObject::nombreVertices()
{
    return m_nombrePointFace;
}

void LoadObject::afficherTexte(glm::mat4 &projection, glm::mat4 &modelview)
{
    float verticesTexture[] = {-0.5, 0.3,   0.5, 0.3,   0.5, -0.3,
                               -0.5, 0.3,   -0.5, -0.3,   0.5, -0.3};

    float texture[] = {0, 1,   1, 1,   1, 0,
                       0, 1,   0, 0,   1, 0};

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glUseProgram(m_shader.getProgramID());

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, verticesTexture);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, texture);
    glEnableVertexAttribArray(2);

    glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, glm::value_ptr(modelview));

    glBindTexture(GL_TEXTURE_2D, m_texture.getID());

    glDrawArrays(GL_TRIANGLES, 0, 6);

    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glUseProgram(0);

    glDisable(GL_BLEND);
}

void LoadObject::afficherAvecTextures(glm::mat4 &projection, glm::mat4 &modelview)
{
    // Activation du shader
    glUseProgram(m_shader.getProgramID());

    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(0);

    // Envoi des vertices

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, &m_tableauObjVertices[0]);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, &m_tableauObjTexture[0]);

    // Envoi des matrices
    glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, glm::value_ptr(modelview));


    // Verrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, m_texture.getID());

    // Rendu
    glDrawArrays(GL_TRIANGLES, 0, nombreVertices());

    // D�verrouillage de la texture
    glBindTexture(GL_TEXTURE_2D, 0);

    // D�sactivation des tableaux
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(0);

    // D�sactivation du shader

    glUseProgram(0);
}

std::vector<float> LoadObject::getTableauObjVertices()
{
    return m_tableauObjVertices;
}
