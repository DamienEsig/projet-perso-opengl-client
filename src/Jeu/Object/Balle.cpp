#include "Balle.h"

/**
** La classe Balle permet de cr�er une balle cr��e par un Joueur
**/

Balle::Balle() : m_orientation(glm::vec3(0, 0, 0)), m_enMouvement(true)
{
    //ctor
}

Balle::~Balle()
{
    //dtor
}

void Balle::setOrientation(glm::vec3 orientation)
{
    m_orientation = orientation;
}

glm::vec3 Balle::getOrientation()
{
    return m_orientation;
}

void Balle::setMouvement(bool mouvement)
{
    m_enMouvement = mouvement;
}

bool Balle::getMouvement()
{
    return m_enMouvement;
}
