#include "Jeu/Object/Object.h"

/**
** La classe Object permet de cr�er un objet 3D
**/

Object::Object() : m_canBeControlled(false), m_hauteurCamera(0.0f), m_attacheSol(false), m_angleTheta(0.0f), m_collision(nullptr), m_connexion(nullptr)
{
    //ctor
}

Object::~Object()
{
    //dtor
}

void Object::setPosition(float x, float y, float z)
{
    setPosition(glm::vec3(x, y, z));
}

void Object::setPosition(glm::vec3 vec3)
{
    m_position = vec3;
}

void Object::setLoadObject(LoadObject* loadObject)
{
    m_loadObject = loadObject;
}

void Object::setBody(std::string pathFile)
{
    m_loadObject = new LoadObject();
    if(!pathFile.empty())
    {
        m_loadObject->setPathFile(pathFile);
        m_loadObject->loadMesh();
    }
}

void Object::setShader(std::string vertexShader, std::string fragmentShader)
{
    m_loadObject->loadShader(vertexShader, fragmentShader);
}

void Object::setTexture(std::string pathTexture)
{
    m_loadObject->loadTexture(pathTexture);
}

void Object::setAngle(float angle)
{
    m_angleTheta = angle;
}

void Object::afficherAvecTextures(glm::mat4& projection, glm::mat4& modelview)
{
    glm::mat4 savedModelview = modelview;
    modelview = glm::translate(modelview, m_position);
    modelview = glm::rotate(modelview, m_angleTheta, glm::vec3(0.0, 1.0, 0.0));
    m_loadObject->afficherAvecTextures(projection, modelview);
    modelview = savedModelview;
}

glm::vec3* Object::getPosition()
{
    return &m_position;
}

float* Object::getTheta()
{
    return &m_angleTheta;
}

bool Object::getCanBeControlled()
{
    return m_canBeControlled;
}

void Object::setCanBeControlled(bool cdc)
{
    m_canBeControlled = cdc;
}

float Object::getHauteurCamera()
{
    return m_hauteurCamera;
}

void Object::setHauteurCamera(float hauteurCam)
{
    m_hauteurCamera = hauteurCam;
}

bool Object::getAttacheSol()
{
    return m_attacheSol;
}

void Object::setAttacheSol(bool as)
{
    m_attacheSol = as;
}

void Object::action(Input input)
{

}

void Object::setCollision(Collision* collision)
{
    m_collision = collision;
}

void Object::setConnexion(ConnexionServeur* connexion)
{
    m_connexion = connexion;
}

