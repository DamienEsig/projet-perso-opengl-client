#include "Texte3D.h"

/**
** Classe permettant d'afficher un texte en 3D
**/

Texte3D::Texte3D(std::string texte) : m_texte(texte)
{
    //ctor
}

Texte3D::~Texte3D()
{
    //dtor
}

void Texte3D::setTexte(std::string texte)
{
    m_texte = texte;
}

void Texte3D::setTexture()
{
    m_loadObject->loadTextureSDLSurface(m_texte);
}

void Texte3D::afficher(glm::mat4& projection, glm::mat4& modelview)
{
    glm::mat4 savedModelview = modelview;
    m_loadObject->afficherTexte(projection, modelview);
    modelview = savedModelview;
}
