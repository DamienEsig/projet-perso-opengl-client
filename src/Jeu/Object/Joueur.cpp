#include "Joueur.h"

/**
** La classe Joueur permet de cr�er un Joueur
**/

Joueur::Joueur(glm::vec3 position, float orientation, std::string pseudo) : m_pseudo(pseudo), m_textePseudo(m_pseudo), m_listeBalle(), m_tirer(false), m_stop(false)
{
    m_position = position;
    m_angleTheta = orientation;
    m_attacheSol = true;
    m_hauteurCamera = 2.2f;
    loadPseudoSurface();
    m_balleObject.setPathFile("objets/balle.obj");
    m_balleObject.loadMesh();
    m_balleObject.loadShader("shaders/texture.vert", "shaders/texture.frag");
    m_balleObject.loadTexture("textures/bois.jpg");
    std::thread(&Joueur::mouvementBalle, this).detach();
}

Joueur::~Joueur()
{
    //dtor
}

void Joueur::setCollision(Collision* collision)
{
    m_collision = collision;
}

// Fonction permettant de cr�er un texte
void Joueur::loadPseudoSurface()
{
    m_textePseudo.setBody("");
    m_textePseudo.setPosition(glm::vec3(0, 0, 0));
    m_textePseudo.setTexture();
    m_textePseudo.setShader("shaders/texture.vert", "shaders/texture.frag");
}

void Joueur::afficherAvecTextures(glm::mat4& projection, glm::mat4& modelview)
{
    glm::mat4 savedModelview = modelview;
    modelview = glm::translate(modelview, m_position);
    modelview = glm::rotate(modelview, m_angleTheta, glm::vec3(0.0, 1.0, 0.0));
    m_loadObject->afficherAvecTextures(projection, modelview);
    modelview = glm::translate(modelview, glm::vec3(0, 3, 0));
    m_textePseudo.afficher(projection, modelview);
    modelview = savedModelview;

    // Affiche les nalles de la liste m_listeBalle
    std::list<Balle*>::iterator it;
    for(it = m_listeBalle.begin(); it != m_listeBalle.end(); it++)
        (*it)->afficherAvecTextures(projection, modelview);
}

void Joueur::setPseudo(std::string pseudo)
{
    m_pseudo = pseudo;
    m_textePseudo.setTexte(m_pseudo);
    loadPseudoSurface();
}

std::string Joueur::getPseudo()
{
    return m_pseudo;
}

void Joueur::action(Input input)
{
    if(input.getBoutonSouris(SDL_BUTTON_LEFT))
    {
        m_tirer = true;
    }
}

// Fonction permettant de cr�er un objet balle et de l'ajouter � la liste
void Joueur::tirer(glm::vec3* orientation)
{
    while(!m_stop)
    {
        if(m_tirer)
        {
            Balle* balle = new Balle();
            balle->setOrientation(*orientation);
            balle->setCollision(m_collision);
            balle->setLoadObject(&m_balleObject);
            balle->setPosition(getPosition()->x, getPosition()->y + m_hauteurCamera, getPosition()->z);
            m_listeBalle.push_back(balle);
            m_tirer = false;
            if(m_connexion != nullptr)
                m_connexion->setSendBalle(true);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
}

// Fonction permettant de donner un mouvement aux balles pr�sentes dans la liste
void Joueur::mouvementBalle()
{
    std::list<Balle*>::iterator it;
    float m_vitesse = 0.1f;
    while(!m_stop)
    {
        for(it = m_listeBalle.begin(); it != m_listeBalle.end(); it++)
        {
            if((m_collision->collisionTerrain((*it)->getPosition()->x + (*it)->getOrientation().x * m_vitesse, (*it)->getPosition()->y, (*it)->getPosition()->z + (*it)->getOrientation().z * m_vitesse) == false))
                (*it)->setPosition((*it)->getPosition()->x + (*it)->getOrientation().x * m_vitesse, (*it)->getPosition()->y + (*it)->getOrientation().y * m_vitesse, (*it)->getPosition()->z + (*it)->getOrientation().z * m_vitesse);
            else
            {
                (*it)->setMouvement(false);
                it = m_listeBalle.erase(it);
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void Joueur::stop()
{
    m_stop = true;
}

// Ajoute une balle re�ue du serveur dans la liste
void Joueur::addBalleListe(socketBalle* balleSocket, int* etat)
{
    while(!m_stop)
    {
        if(*etat == 2)
        {
            Balle* balle = new Balle();
            balle->setOrientation(glm::vec3(balleSocket->oriX, balleSocket->oriY, balleSocket->oriZ));
            balle->setCollision(m_collision);
            balle->setLoadObject(&m_balleObject);
            balle->setPosition(glm::vec3(balleSocket->posX, balleSocket->posY + m_hauteurCamera, balleSocket->posZ));
            m_listeBalle.push_back(balle);
            *etat = 0;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}
