#include "Camera.h"
#include <iostream>

/**
** Classe utilis�e pour la gestion de la cam�ra
**/

Camera::Camera() : m_phi(0.0), m_theta(0.0), m_orientation(), m_axeVertical(0, 0, 1), m_deplacementLateral(), m_position(), m_pointCible(), m_cs(nullptr),
                   m_lastSocketSend(false), m_object(nullptr)
{

}

Camera::Camera(glm::vec3 position, glm::vec3 pointCible, glm::vec3 axeVertical) : m_phi(0.0), m_theta(0.0), m_orientation(),
                                                                                  m_axeVertical(axeVertical), m_deplacementLateral(),
                                                                                  m_position(position), m_pointCible(pointCible),
                                                                                  m_vitesse(0), m_sensibilite(0), m_cs(nullptr),
                                                                                  m_lastSocketSend(false), m_object(nullptr)
{
    // Actualisation du point cibl�

    setPointcible(pointCible);
}

Camera::~Camera()
{
    //dtor
}

void Camera::setPosition(glm::vec3 position, glm::vec3 pointCible, glm::vec3 axeVertical)
{
    m_position = position;
    setPointcible(pointCible);
    m_axeVertical = axeVertical;
}

void Camera::orienter(int xRel, int yRel)
{
    // R�cup�ration des angles

    m_phi += -yRel * m_sensibilite;
    m_theta += -xRel * m_sensibilite;


    // Limitation de l'angle phi

    if(m_phi > 89.0)
        m_phi = 89.0;

    else if(m_phi < -89.0)
        m_phi = -89.0;


    // Conversion des angles en radian

    float phiRadian = m_phi * M_PI / 180;
    float thetaRadian = m_theta * M_PI / 180;


    // Si l'axe vertical est l'axe X

    if(m_axeVertical.x == 1.0)
    {
        // Calcul des coordonn�es sph�riques

        m_orientation.x = sin(phiRadian);
        m_orientation.y = cos(phiRadian) * cos(thetaRadian);
        m_orientation.z = cos(phiRadian) * sin(thetaRadian);
    }


    // Si c'est l'axe Y

    else if(m_axeVertical.y == 1.0)
    {
        // Calcul des coordonn�es sph�riques

        m_orientation.x = cos(phiRadian) * sin(thetaRadian);
        m_orientation.y = sin(phiRadian);
        m_orientation.z = cos(phiRadian) * cos(thetaRadian);
    }


    // Sinon c'est l'axe Z

    else
    {
        // Calcul des coordonn�es sph�riques

        m_orientation.x = cos(phiRadian) * cos(thetaRadian);
        m_orientation.y = cos(phiRadian) * sin(thetaRadian);
        m_orientation.z = sin(phiRadian);
    }


    // Calcul de la normale

    m_deplacementLateral = glm::cross(m_axeVertical, m_orientation);
    m_deplacementLateral = glm::normalize(m_deplacementLateral);


    // Calcul du point cibl� pour OpenGL

    m_pointCible = m_position + m_orientation;
}

void Camera::setConnexionServeur(ConnexionServeur* cs)
{
    m_cs = cs;
    m_lastSocketSend = true;
    std::thread(&Camera::sendPos, this).detach();
}

void Camera::deplacer(Input const &input, info par, Collision collision)
{
    if(m_object != nullptr)
    {
        if(m_object->getCanBeControlled())
            actionDeplacer(input, par, collision);
    }
    else
        actionDeplacer(input, par, collision);

    // Gestion de l'orientation

    if(input.mouvementSouris())
    {
        orienter(input.getXRel(), input.getYRel());
        if(m_object != nullptr)
            m_object->setAngle(m_theta * 0.0174532925);
        if(m_lastSocketSend == false && m_cs != nullptr)
            m_lastSocketSend = true;
    }
}

void Camera::actionDeplacer(Input const &input, info par, Collision collision)
{
    // Avanc�e de la cam�ra

    if(input.getTouche(SDL_SCANCODE_UP) || input.getTouche((SDL_Scancode)par.avancer))
    {
        if((collision.collisionTerrain(m_position.x + m_orientation.x * m_vitesse, m_position.y, m_position.z + m_orientation.z * m_vitesse) == false))
        {
            m_position.x = m_position.x + m_orientation.x * m_vitesse;
            m_position.z = m_position.z + m_orientation.z * m_vitesse;
            m_pointCible = m_position + m_orientation;

            if(m_object != nullptr)
            {
                if(m_object->getAttacheSol())
                    m_position.y = m_object->getHauteurCamera();
                else
                    m_position.y = m_position.y + m_orientation.y * m_vitesse;
                m_object->setPosition(m_position.x, m_object->getPosition()->y, m_position.z);
            }
            else
                m_position.y = m_position.y + m_orientation.y * m_vitesse;

            if(m_lastSocketSend == false && m_cs != nullptr)
                m_lastSocketSend = true;
        }
    }


    // Recul de la cam�ra

    if(input.getTouche(SDL_SCANCODE_DOWN) || input.getTouche((SDL_Scancode)par.reculer))
    {
        if((collision.collisionTerrain(m_position.x - m_orientation.x * m_vitesse, m_position.y, m_position.z - m_orientation.z * m_vitesse) == false))
        {
            m_position.x = m_position.x - m_orientation.x * m_vitesse;
            m_position.z = m_position.z - m_orientation.z * m_vitesse;
            m_pointCible = m_position + m_orientation;
            if(m_object != nullptr)
            {
                if(m_object->getAttacheSol())
                    m_position.y = m_object->getHauteurCamera();
                else
                    m_position.y = m_position.y - m_orientation.y * m_vitesse;
                m_object->setPosition(m_position.x, m_object->getPosition()->y, m_position.z);
            }
            else
                m_position.y = m_position.y - m_orientation.y * m_vitesse;

            if(m_lastSocketSend == false && m_cs != nullptr)
                m_lastSocketSend = true;
        }
    }


    // D�placement vers la gauche

    if(input.getTouche(SDL_SCANCODE_LEFT) || input.getTouche((SDL_Scancode)par.gauche))
    {
        if((collision.collisionTerrain(m_position.x + m_deplacementLateral.x * m_vitesse, m_position.y, m_position.z + m_deplacementLateral.z * m_vitesse) == false))
        {
            m_position.x = m_position.x + m_deplacementLateral.x * m_vitesse;
            m_position.z = m_position.z + m_deplacementLateral.z * m_vitesse;
            m_pointCible = m_position + m_orientation;
            if(m_object != nullptr)
            {
                if(m_object->getAttacheSol())
                    m_position.y = m_object->getHauteurCamera();
                else
                    m_position.y = m_position.y + m_deplacementLateral.y * m_vitesse;
                m_object->setPosition(m_position.x, m_object->getPosition()->y, m_position.z);
            }
            else
                m_position.y = m_position.y - m_deplacementLateral.y * m_vitesse;

            if(m_lastSocketSend == false && m_cs != nullptr)
                m_lastSocketSend = true;
        }
    }


    // D�placement vers la droite

    if(input.getTouche(SDL_SCANCODE_RIGHT) || input.getTouche((SDL_Scancode)par.droite))
    {
        if((collision.collisionTerrain(m_position.x - m_deplacementLateral.x * m_vitesse, m_position.y, m_position.z - m_deplacementLateral.z * m_vitesse) == false))
        {
            m_position.x = m_position.x - m_deplacementLateral.x * m_vitesse;
            m_position.z = m_position.z - m_deplacementLateral.z * m_vitesse;
            m_pointCible = m_position + m_orientation;
            if(m_object != nullptr)
            {
                if(m_object->getAttacheSol())
                    m_position.y = m_object->getHauteurCamera();
                else
                    m_position.y = m_position.y - m_deplacementLateral.y * m_vitesse;
                m_object->setPosition(m_position.x, m_object->getPosition()->y, m_position.z);
            }
            else
                m_position.y = m_position.y - m_deplacementLateral.y * m_vitesse;

            if(m_lastSocketSend == false && m_cs != nullptr)
                m_lastSocketSend = true;
        }
    }
}

// Envoie de la position au serveur
void Camera::sendPos()
{
    while(m_cs->getEtat())
    {
        if(m_lastSocketSend)
        {
            m_cs->setSendPos(true);
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            m_lastSocketSend = false;
        }
    }

}

void Camera::lookAt(glm::mat4 &modelview)
{
    // Actualisation de la vue dans la matrice

    modelview = glm::lookAt(m_position, m_pointCible, m_axeVertical);
}

void Camera::setPointcible(glm::vec3 pointCible)
{
    // Calcul du vecteur orientation

    m_orientation = m_pointCible - m_position;
    m_orientation = glm::normalize(m_orientation);


    // Si l'axe vertical est l'axe X

    if(m_axeVertical.x == 1.0)
    {
        // Calcul des angles

        m_phi = asin(m_orientation.x);
        m_theta = acos(m_orientation.y / cos(m_phi));

        if(m_orientation.y < 0)
            m_theta *= -1;
    }


    // Si c'est l'axe Y

    else if(m_axeVertical.y == 1.0)
    {
        // Calcul des angles

        m_phi = asin(m_orientation.y);
        m_theta = acos(m_orientation.z / cos(m_phi));

        if(m_orientation.z < 0)
            m_theta *= -1;
    }


    // Sinon c'est l'axe Z

    else
    {
        // Calcul des angles

        m_phi = asin(m_orientation.x);
        m_theta = acos(m_orientation.z / cos(m_phi));

        if(m_orientation.z < 0)
            m_theta *= -1;
    }


    // Conversion en degr�s

    m_phi = m_phi * 180 / M_PI;
    m_theta = m_theta * 180 / M_PI;
}

float Camera::getSensibilite() const
{
    return m_vitesse;
}


float Camera::getVitesse() const
{
    return m_vitesse;
}


glm::vec3* Camera::getOrientation()
{
    return &m_orientation;
}


void Camera::setSensibilite(float sensibilite)
{
    m_sensibilite = sensibilite;
}


void Camera::setVitesse(float vitesse)
{
    m_vitesse = vitesse;
}

void Camera::setObjectToWatch(Object* object)
{
    m_object = object;
    m_position = (*object->getPosition());
    m_position.y = m_object->getHauteurCamera();
    m_theta = (*object->getTheta());
}

Object* Camera::getObjectToWatch()
{
    return m_object;
}
