#include "Jeu/SceneOpenGL.h"
using namespace glm;

/**
** Classe permettant de cr�er la sc�ne openGL
**/

SceneOpenGL::SceneOpenGL() : m_cs(nullptr)
{

}

SceneOpenGL::~SceneOpenGL()
{
    SDL_GL_DeleteContext(m_contexteOpenGL);
}

void SceneOpenGL::setWindow(SDL_Window* fenetre)
{
    m_fenetre = fenetre;
}

void SceneOpenGL::setParam(info par)
{
    m_par = par;
}

void SceneOpenGL::setConnexionServeur(ConnexionServeur* cs)
{
    m_cs = cs;
    m_camera.setConnexionServeur(m_cs);
}

bool SceneOpenGL::initialiserFenetre()
{
    // Version d'OpenGL
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    // Double Buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    m_contexteOpenGL = SDL_GL_CreateContext(m_fenetre);

    if(m_contexteOpenGL == 0)
    {
        std::cout << SDL_GetError() << std::endl;
        SDL_DestroyWindow(m_fenetre);
        SDL_Quit();

        return false;
    }
    return true;
}

bool SceneOpenGL::initGL()
{

    GLenum initialisationGLEW(glewInit());

    if(initialisationGLEW != GLEW_OK)
    {
        std::cout << "Erreur d'initialisation de GLEW : " << glewGetErrorString(initialisationGLEW) << std::endl;

        SDL_GL_DeleteContext(m_contexteOpenGL);
        SDL_DestroyWindow(m_fenetre);
        SDL_Quit();

        return false;
    }

    glEnable(GL_DEPTH_TEST);

    return true;
}

int SceneOpenGL::bouclePrincipale()
{
    Uint32 frameRate (1000 / 40);
    Uint32 debutBoucle(0), finBoucle(0), tempsEcoule(0);

    // Boolean terminer

    bool terminer(false);

    // Matrices

    mat4 projection;
    mat4 modelview;

    // Chargement des objets
    LoadObject playerLoadedObject = LoadObject();
    playerLoadedObject.setPathFile("objets/perso_test.obj");
    playerLoadedObject.loadMesh();
    playerLoadedObject.loadShader("shaders/texture.vert", "shaders/texture.frag");
    playerLoadedObject.loadTexture("textures/bois.jpg");

    //Reception de la position des joueurs
    m_listeJoueur = std::list<positionJoueur>();
    if(m_cs != nullptr)
    {
        std::thread reception = std::thread(&ConnexionServeur::reception, m_cs, &m_listeJoueur);
        reception.detach();
    }



    projection = perspective(70.0, (double) 800 / 600, 1.0, 1000.0);
    modelview = mat4(1.0);

    Collision collision = Collision();

    Terrain terrain = Terrain();
    terrain.setSol("objets/sol.obj");
    terrain.setMur("objets/mur.obj");
    terrain.setPosition(vec3(0, 0, 0));
    terrain.setTextureSol("textures/sol.jpg");
    terrain.setTextureMur("textures/mur.jpg");

    collision.setTerrain(terrain.getTableauVertices(), terrain.getNombreVertices());

    terrain.setShader("shaders/texture.vert", "shaders/texture.frag");


    m_camera.setPosition(vec3(0, 2.2, 0), vec3(0, 0, 0), vec3(0, 1, 0));
    m_camera.setVitesse(0.1);
    m_camera.setSensibilite(1);

    // Capture du pointeur

    m_input = Input();
    m_input.afficherPointeur(false);
    m_input.capturerPointeur(true);

    // Boucle principale

    Joueur* object = new Joueur(vec3(0, 0, 0), 0, " ");
    object->setLoadObject(&playerLoadedObject);
    object->setCanBeControlled(true);
    object->setCollision(&collision);
    object->setConnexion(m_cs);

    m_camera.setObjectToWatch(object);

    Joueur* newPlayer = new Joueur(vec3(0, 0, 0), 0, " ");
    newPlayer->setLoadObject(&playerLoadedObject);
    newPlayer->setCollision(&collision);
    newPlayer->setConnexion(m_cs);

    std::thread(&Joueur::tirer, object, m_camera.getOrientation()).detach();
    if(m_cs != nullptr)
    {
        std::thread(&ConnexionServeur::envoiePos, m_cs, object->getPosition(), object->getTheta()).detach();
        std::thread(&ConnexionServeur::envoieBalle, m_cs, object->getPosition(), m_camera.getOrientation(), object->getHauteurCamera()).detach();
        std::thread(&Joueur::addBalleListe, newPlayer, m_cs->getBalle(), m_cs->getEtatBalle()).detach();
    }

    while(!terminer)
    {
        debutBoucle = SDL_GetTicks();

        if(m_evenements.window.event == SDL_WINDOWEVENT_CLOSE)
            terminer = true;

        // Gestion des �v�nements

        m_input.updateEvenements();

        if(m_input.getTouche(SDL_SCANCODE_ESCAPE))
        {
            terminer = true;
            break;
        }

        if(Joueur* joueur = dynamic_cast<Joueur*>(m_camera.getObjectToWatch()))
            joueur->action(m_input);
        else
            m_camera.getObjectToWatch()->action(m_input);


        m_camera.deplacer(m_input, m_par, collision);

        // Nettoyage de l'�cran

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        // Gestion de la cam�ra

        m_camera.lookAt(modelview);
        object->afficherAvecTextures(projection, modelview);
        terrain.afficherAvecTextures(projection, modelview);

        if(m_cs != nullptr)
        {
            if(m_listeJoueur.size() != 0)
            {
                std::list<positionJoueur>::iterator it;
                for(it = m_listeJoueur.begin(); it != m_listeJoueur.end(); it++)
                {
                    positionJoueur pj = (*it);
                    if(strcmp(pj.pseudo, "") != 0)
                    {
                        newPlayer->setPosition(vec3(pj.posX, pj.posY, pj.posZ));
                        newPlayer->setAngle(pj.angle * 0.0174532925);
                        newPlayer->setPseudo(pj.pseudo);
                        newPlayer->afficherAvecTextures(projection, modelview);
                    }
                    else
                        it = m_listeJoueur.erase(it);
                }
            }
        }

        // Actualisation de la fen�tre

        SDL_GL_SwapWindow(m_fenetre);

        if(m_cs != nullptr)
            if(m_cs->getEtat() == false)
                terminer = true;

        finBoucle = SDL_GetTicks();
        tempsEcoule = finBoucle - debutBoucle;

        if(tempsEcoule < frameRate)
            SDL_Delay(frameRate - tempsEcoule);
    }
    m_input.afficherPointeur(true);
    m_input.capturerPointeur(false);
    if(m_cs != nullptr)
        m_cs->stop();
    object->stop();
    newPlayer->stop();
    SDL_DestroyWindow(m_fenetre);
    return 0;
}
