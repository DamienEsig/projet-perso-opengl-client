#ifndef CONNEXIONSERVEUR_H
#define CONNEXIONSERVEUR_H

#if defined (WIN32)
    #include <winsock2.h>
    typedef int socklen_t;
#elif defined (linux)
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <unistd.h>
    #define INVALID_SOCKET -1
    #define SOCKET_ERROR -1
    #define closesocket(s) close(s)
    typedef int SOCKET;
    typedef struct sockaddr_in SOCKADDR_IN;
    typedef struct sockaddr SOCKADDR;
#endif

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>


#include <chrono>
#include <list>
#include <stdio.h>
#include <iostream>
#include <string>
#include <thread>
#include <winsock2.h>

#define PORT 23

struct positionJoueur
{
    char pseudo[20];
    float posX, posY, posZ, angle;
}typedef positionJoueur;

struct socketBalle
{
    float posX, posY, posZ, oriX, oriY, oriZ;
}typedef socketBalle;

struct socketAll
{
    SOCKET sockDeco;
    SOCKET sockBalle;
    SOCKET sockPos;
    SOCKET sockSentinelle;
}typedef socketAll;

class ConnexionServeur
{
    public:
        ConnexionServeur(std::string);
        virtual ~ConnexionServeur();
        socketAll startConnexion();
        int getConnexion();
        void envoiePseudo(std::string);
        void startSentinelle();
        void startSentinelleRecep();
        void lastCheck();
        void stop();
        bool getEtat();
        void envoiePos(glm::vec3*, float*);
        void envoieBalle(glm::vec3*, glm::vec3*, float);
        void reception(std::list<positionJoueur>*);

        bool getSendBalle();
        void setSendBalle(bool);

        bool getSendPos();
        void setSendPos(bool);

        socketBalle* getBalle();
        void deplacementThread(std::list<positionJoueur>*);
        void deconnectionThread(std::list<positionJoueur>*);
        void balleThread();

        int* getEtatBalle();
    protected:
    private:
        std::string m_ip;
        int m_coOk;
        socketAll m_sockAll;
        std::thread sentinelle;
        std::thread sentinelleRecep;
        bool m_sentinelle;
        std::chrono::system_clock::time_point m_lastCheck;
        std::string m_pseudo;
        bool m_sendBalle;
        bool m_sendPos;
        socketBalle m_socketBalle;
        int* m_balle;
};

#endif // CONNEXIONSERVEUR_H
