#ifndef TEXTE3D_H
#define TEXTE3D_H

#include <Object.h>
#include <string>


class Texte3D : public Object
{
    public:
        Texte3D(std::string);
        virtual ~Texte3D();
        void setTexte(std::string);
        void setTexture();
        void afficher(glm::mat4&, glm::mat4&);
    protected:
    private:
        std::string m_texte;
};

#endif // TEXTE3D_H
