#ifndef BALLE_H
#define BALLE_H

#include <Object.h>
#include <thread>
#include <list>

class Balle : public Object
{
    public:
        Balle();
        virtual ~Balle();
        glm::vec3 getOrientation();
        void setOrientation(glm::vec3);
        void setMouvement(bool);
        bool getMouvement();
    protected:
    private:
        glm::vec3 m_orientation;
        bool m_enMouvement;
};

#endif // BALLE_H
