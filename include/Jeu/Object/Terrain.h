#ifndef TERRAIN_H
#define TERRAIN_H

#include <Object.h>


class Terrain : public Object
{
    public:
        Terrain();
        virtual ~Terrain();
        std::vector<float> getTableauVertices();
        int getNombreVertices();
        void setSol(std::string);
        void setMur(std::string);
        void setShader(std::string, std::string);
        void setTextureSol(std::string);
        void setTextureMur(std::string);
        void afficherAvecTextures(glm::mat4&, glm::mat4&);
    protected:
    private:
        LoadObject m_loadObjectSol;
        LoadObject m_loadObjectMur;
};

#endif // TERRAIN_H
