#ifndef LOADOBJECT_H
#define LOADOBJECT_H

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <SDL_ttf.h>
#include <vector>

#include "Jeu/Object/Shader.h"
#include "Jeu/Object/Texture.h"


class LoadObject
{
    public:
        LoadObject();
        virtual ~LoadObject();
        void setPathFile(std::string);
        void loadMesh();
        void loadShader(std::string, std::string);
        void loadTexture(std::string);
        void afficherAvecTextures(glm::mat4&, glm::mat4&);
        void loadTextureSDLSurface(std::string);
        void afficherTexte(glm::mat4&, glm::mat4&);
        std::vector<float> getTableauObjVertices();
        int nombreVertices();
    protected:
    private:
        std::string m_pathFile;
        Shader m_shader;
        Texture m_texture;
        int m_nombrePointFace;
        std::vector<float> m_tableauObjVertices;
        std::vector<float> m_tableauObjTexture;
        std::vector<float> m_tableauObjNormales;
};

#endif // LOADOBJECT_H
