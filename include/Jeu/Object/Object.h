#ifndef OBJECT_H
#define OBJECT_H

#include "LoadObject.h"
#include "Menu/Input.h"
#include "Jeu/Collision.h"
#include "ConnexionServeur.h"

class Object
{
    public:
        Object();
        virtual ~Object();
        void setPosition(float, float, float);
        void setPosition(glm::vec3);
        void setLoadObject(LoadObject*);
        void setBody(std::string);
        void setShader(std::string, std::string);
        void setTexture(std::string);
        void afficherAvecTextures(glm::mat4&, glm::mat4&);

        void setAngle(float);
        glm::vec3* getPosition();
        float* getTheta();
        void setCanBeControlled(bool);
        bool getCanBeControlled();
        void setHauteurCamera(float);
        float getHauteurCamera();
        bool getAttacheSol();
        void setAttacheSol(bool);

        void action(Input);
        void setCollision(Collision*);
        void setConnexion(ConnexionServeur*);
    protected:
        LoadObject* m_loadObject;
        glm::vec3 m_position;
        float m_angleTheta;
        bool m_canBeControlled;
        float m_hauteurCamera;
        bool m_attacheSol;
        Collision* m_collision;
        ConnexionServeur* m_connexion;
    private:
};

#endif // OBJECT_H
