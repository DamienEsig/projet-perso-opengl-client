#ifndef JOUEUR_H
#define JOUEUR_H

#include <Object.h>
#include <Texte3D.h>
#include <Balle.h>
#include <Collision.h>
#include <list>
#include <thread>


class Joueur : public Object
{
    public:
        Joueur(glm::vec3, float, std::string);
        virtual ~Joueur();
        void loadPseudoSurface();
        void afficherAvecTextures(glm::mat4&, glm::mat4&);
        std::string getPseudo();
        void setPseudo(std::string);
        void setCollision(Collision*);
        void action(Input);
        void tirer(glm::vec3*);
        void mouvementBalle();
        void stop();
        void addBalleListe(socketBalle*, int*);
    protected:
    private:
        std::string m_pseudo;
        Texte3D m_textePseudo;
        std::list<Balle*> m_listeBalle;
        LoadObject m_balleObject;
        bool m_tirer;
        bool m_stop;
};

#endif // JOUEUR_H
