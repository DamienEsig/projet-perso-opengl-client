#ifndef SCENEOPENGL_H
#define SCENEOPENGL_H

#include <GL/glew.h>

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <SDL2/SDL.h>
#include <iostream>
#include <list>
#include "Menu/Param.h"
#include "Menu/Input.h"
#include "Jeu/Object/Shader.h"
#include "Jeu/Object/Joueur.h"
#include "Jeu/Object/Terrain.h"
#include "Jeu/Camera.h"
#include "Jeu/ConnexionServeur.h"


class SceneOpenGL
{
public:
    SceneOpenGL();
    virtual ~SceneOpenGL();
    bool initGL();
    bool initialiserFenetre();
    int bouclePrincipale();
    void setWindow(SDL_Window*);
    void setParam(info);
    void setConnexionServeur(ConnexionServeur*);
protected:
private:
    SDL_Window* m_fenetre;
    SDL_Event m_evenements;
    SDL_GLContext m_contexteOpenGL;
    info m_par;
    Input m_input;
    ConnexionServeur* m_cs;
    Camera m_camera;
    std::list<positionJoueur> m_listeJoueur;
};

#endif // SCENEOPENGL_H
