#ifndef COLLISION_H
#define COLLISION_H

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <vector>

struct Point {
    float X;
    float Y;
    float Z;
}typedef Point;

struct Triangle {
    Point point1;
    Point point2;
    Point point3;
}typedef Triangle;

class Collision
{
    public:
        Collision();
        virtual ~Collision();
        void setTerrain(std::vector<float>, int);
        bool collisionTerrain(float , float , float);
        float getDistanceToPlane(Triangle, Point);
        bool pointInTriangle(Point, Point, Point, Point);
        bool sameSide(Point, Point, Point, Point);
    protected:
    private:
        std::vector<float> m_terrainVertices;
        int m_vertices;
};

#endif // COLLISION_H
