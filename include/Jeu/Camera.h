#ifndef CAMERA_H
#define CAMERA_H

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#include "Menu/Input.h"
#include "Menu/Param.h"
#include "Jeu/ConnexionServeur.h"
#include "Object.h"

class Camera
{
    public:

    Camera();
    Camera(glm::vec3, glm::vec3, glm::vec3);
    ~Camera();
    void setPosition(glm::vec3, glm::vec3, glm::vec3);
    void orienter(int, int);
    void deplacer(Input const&, info, Collision);
    void lookAt(glm::mat4&);
    void setPointcible(glm::vec3);

    float getSensibilite() const;
    float getVitesse() const;
    glm::vec3* getOrientation();

    void setSensibilite(float);
    void setVitesse(float);

    void setConnexionServeur(ConnexionServeur*);
    void sendPos();

    void setObjectToWatch(Object* object);
    Object* getObjectToWatch();

    void actionDeplacer(Input const&, info, Collision);

    private:

    float m_phi;
    float m_theta;
    glm::vec3 m_orientation;

    glm::vec3 m_axeVertical;
    glm::vec3 m_deplacementLateral;

    glm::vec3 m_position;
    glm::vec3 m_pointCible;

    float m_sensibilite;
    float m_vitesse;

    ConnexionServeur* m_cs;
    bool m_lastSocketSend;

    Object* m_object;
};

#endif // CAMERA_H
