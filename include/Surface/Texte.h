#ifndef TEXTE_H
#define TEXTE_H

#include <SDL_ttf.h>
#include <string.h>
#include <iostream>

class Texte
{
public:
    Texte(const std::string&, const std::string&, SDL_Color, int, SDL_Renderer*);
    virtual ~Texte();
    void afficher(int, int);
    void afficher(int, int, int, int);
    void setMessage(const std::string&);
    bool click();
    bool hover();
    int getLength();
protected:
private:
    SDL_Texture* renderText(const std::string&, const std::string&, SDL_Color, int, SDL_Renderer*);
    void renderTexture(SDL_Texture*, SDL_Renderer*, int, int, int, int);
    void renderTexture(SDL_Texture*, SDL_Renderer*, int , int );
    void logSDLError(std::ostream &os, const std::string &msg);
    SDL_Renderer *m_renderer;
    std::string m_message;
    std::string m_fontFile;
    SDL_Color m_color;
    int m_fontSize;
    SDL_Texture *m_tex;
    SDL_Rect m_dst;
};

#endif // TEXTE_H
