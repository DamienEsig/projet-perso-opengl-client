#ifndef IMAGE_H
#define IMAGE_H

#include <SDL2/SDL_image.h>
#include <iostream>


class Image
{
public:
    Image(const std::string&, SDL_Renderer*);
    virtual ~Image();
    void afficher(int, int);
    void afficher(int, int, int, int);
    bool click();
protected:
private:
    SDL_Texture* loadTexture(const std::string&, SDL_Renderer*);
    void renderTexture(SDL_Texture*, SDL_Renderer*, int, int, int, int);
    void renderTexture(SDL_Texture*, SDL_Renderer*, int, int);
    void logSDLError(std::ostream&, const std::string&);
    SDL_Renderer *m_ren;
    SDL_Texture *m_tex;
    SDL_Rect m_dst;
};

#endif // IMAGE_H
