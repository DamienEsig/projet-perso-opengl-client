#ifndef PARAM_H
#define PARAM_H

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <map>

struct info
{
    int tailleFenetreX;
    int tailleFenetreY;
    int reculer;
    int avancer;
    int droite;
    int gauche;
};


class Param
{
public:
    Param();
    virtual ~Param();
    std::string lireParam(char*);
    void update(int, int);
protected:
private:
    void loadParam();
    void updateMap();
    std::map<std::string, std::string> map_params;
};


#endif // PARAM_H
