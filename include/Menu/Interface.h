#ifndef INTERFACE_H
#define INTERFACE_H

#include <SDL2/SDL.h>
#include <iostream>

#include "Jeu/SceneOpenGL.h"
#include "Jeu/ConnexionServeur.h"
#include "Menu/Menu.h"
#include "Menu/Param.h"
#include "Surface/Image.h"
#include "Surface/Texte.h"

using namespace std;

class Interface
{
public:
    Interface();
    virtual ~Interface();
protected:
private:
    void init();
    int menu();
    int options();
    int ipMenu();
    void paramFenetre();
    void updateFenetre();
    int loginIHM();
    int connexionServeur(std::string ipSaisie);
    Param m_param;
    SDL_Window* m_fenetre;
    SDL_Renderer* m_renderer;
    info m_par;
    ConnexionServeur* cs;
    int m_choix;
};

#endif // INTERFACE_H
