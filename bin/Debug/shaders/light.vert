#version 150

uniform mat4 projection;
uniform mat4 modelview;

in vec3 in_Vertex;
in vec2 in_TexCoord0;
in vec3 in_Normal;

out vec3 fragVert;
out vec2 fragTexCoord;
out vec3 fragNormal;

void main() {
    // Pass some variables to the fragment shader
    fragTexCoord = in_TexCoord0;
    fragNormal = in_Normal;
    fragVert = in_Vertex;
    
    // Apply all matrix transformations to vert
    gl_Position = projection * modelview * vec4(in_Vertex, 1);
}